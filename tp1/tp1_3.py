import csv
from tabulate import tabulate



class Date:

    def __init__(self, year=2019, month=1, day=1):
        assert (month >= 1 and month <= 12), "The month must be an integer value between 1 and 12."
        assert (day >= 1 and day <= 31), "The day must be an integer value between 1 and 31."
        self.year = year
        self.month = month
        self.day = day

    # Creates a date from a string
    # formatted as "dd/mm/yyyy"
    @staticmethod
    def dateFromString(dateString):
        args = dateString.split('/')
        date = Date(int(args[2]), int(args[1]), int(args[0]))
        return date

    # overload ==
    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    # Checks if the date equals another one
    def __eq__(self, other):
        if (self.year == other.year
            and self.month == other.month
            and self.day == other.day):
            return True
        return False

    # Checks if the date is before another one
    def __lt__(self, other):
        if (self.year < other.year):
            return True
        elif (self.year == other.year):
            if (self.month < other.month):
                return True
            elif (self.month == other.month):
                if (self.day < other.day):
                    return True
        return False

    # Representation of the date as a string "dd/mm/yyyy"
    def __repr__(self):
        repr = format(self.day, '02') \
            + "/" + format(self.month, '02') \
            + "/" + format(self.year, '02')
        return repr

    # Computes the number of years between the date and another one
    def cal_age(self, date = None):
        if (date is None):
            date = self.__init__()

        age = abs(self.year - date.year)
        if (self.year == date.year):
            if ((self.month == date.month and self.day < date.day)
                    or (self.month < date.month)):
                age -= 1
        return age

class Student:
    nom = ""
    prenom = ""
    age = 0
    email = ""

    def __init__(self, nom, prenom, age):
        self.nom = nom
        self.prenom = prenom
        self.age = age
        self.email = self.prenom.__str__().lower() + '.' + self.nom.__str__().lower() + '@etu.univ-tours.fr'


def parse_date(birth_str):
    list = birth_str.split('/')
    if list == [] :
        raise Exception("Invalid birthday string", birth_str)
    date = Date(list[0], list[1], list[2])
    return date


# def cal_age(str):
#     try:
#         date = parse_date(str)
#     except Exception as err:
#         print(err)
#     else:
#         current_year = datetime.now().year
#         age = int(current_year) - int(date.year)
#         return age
#

def parse_csv(filename):
    list_student =[]
    date = Date(2019, 1, 1)
    with open(filename) as f:
        try:
            f_csv = csv.reader(f)
        except IOError:
            print("Error : Can't find this csv file or fail in reading this file ")
        else:
            for line in f_csv:
                row = line[0].split(';')
                age = Date.dateFromString(row[2]).diffYear(date)
                student = Student(row[0], row[1], age)
                list_student.append(student)
    table = []
    for ele in list_student :
        table.append([ele.nom, ele.prenom, ele.age, ele.email])
    print(tabulate(table, headers=['NOM', 'PRENOM', 'AGE', 'EMAIL']))

if __name__ == '__main__':
    parse_csv("fichetu.csv")

