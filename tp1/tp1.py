import os

# print("Bonjour le monde")

# file name
import sys

name = ""

# file
file = ""


def show_menu(is_chosen, filename):
    print("********************* menu *********************** ")
    if is_chosen:
        print("Now you have chosen the file %s" % filename)
        print("select 1 : re-choose a new file ")
    else:
        print("select 1 : choose a file ")
    print("select 2 : add text in the file chosen by you ")
    print("select 3 : show the file in the terminal ")
    print("select 4 : empty the file ")
    print("select 9 : exit ")


is_chosen = False

while True:
    # create a menu
    show_menu(is_chosen, name)
    try:
        num = input("Input your choice : ")
    except EOFError:
        print("Error : Invalid input")


    # choose a file
    if num == '1':
        print("There are some files that you can choose: ")
        alist = []
        for r, d, f in os.walk(os.getcwd()):
            for fs in f:
                if '.txt' in fs:
                    alist.append(fs)
        for f in alist:
            print(f)
        print("please choose a file or create a new one :")
        name = input()
        print("You have chosen file : " + name)
        is_chosen = True

    # add the text
    elif num == '2':
        try:
            file = open(name, "a+")
        except IOError:
            print("Error : Can't find this file or fail in adding text into this file ")
        else:
            add = input("input your text please : ")
            file.write(add)
            print("You have added the text successfully ")
        finally:
            print("close the file")
            file.close()

    # show in terminal
    elif num == '3':
        try:
            file = open(name, "r+")
        except IOError:
            print("Error : Can't not find this file or fail in reading the file ")
        else:
            print("Here is the content of file %s :" % name)
            for line in file.readlines():
                print(line)
        finally:
            print("close the file")
            file.close()

    # empty the file
    elif num == '4':
        try:
            file = open(name, "w+")
        except IOError:
            print("Error :Can't not find this file or fail in emptying the file  ")
        else:
            file.truncate()
            print("You have emptied it successfully ")
            file.close()

    # invalid input
    elif 5 <= int(num) < 9:
        print("Your input number is invalid, please input again. ")

    # exit the menu
    elif num == '9':
        if file != "":
            file.close()
        print("You quit the program successfully ")
        # exit sys
        sys.exit()
        break


