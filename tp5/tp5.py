import sqlite3
import os
import csv
from tabulate import tabulate
import re
import xml.etree.ElementTree as ET


# def parse_file_csv (file_name) :
#
#

class Region:
    region_code = 0
    region_name = ""
    nb_ardm = 0
    nb_cart = 0
    nb_commu = 0
    pop_muni = 0
    pop_total = 0

    def parse_file_csv(self, filename):
        list_region = []
        header = []
        num = 0
        with open(filename) as f:
            try:
                f_csv = csv.reader(f)
            except IOError:
                print("Error : Can't find this csv file or fail in reading this file ")
            else:
                for row in f_csv:
                    num += 1
                    if num == 7:
                        header = row
                    elif num > 7:
                        reg = Region(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
                        list_region.append(reg)
        return list_region, header


class Department:
    dep_code = 0
    dep_name = ""
    region_code = 0
    nb_ardm = 0
    nb_cart = 0
    nb_commu = 0
    pop_muni = 0
    pop_total = 0

    def parse_file_csv(self, filename):
        list_dept = []
        header = []
        num = 0
        with open(filename) as f:
            try:
                f_csv = csv.reader(f)
            except IOError:
                print("Error : Can't find this csv file or fail in reading this file ")
            else:
                for row in f_csv:
                    num += 1
                    if num == 7:
                        header = [row[2], row[3], row[0], row[4], row[5], row[6], row[7], row[8]];
                    elif num > 7:
                        dep = Department(row[2], row[3], row[0], row[4], row[5], row[6], row[7], row[8])
                        list_dept.append(dep)
        return list_dept, header


class Communes:
    commu_code = 0
    commu_name = ""
    dep_code = 0
    region_code = 0
    ardm_code = 0
    cart_code = 0
    pop_muni = 0
    pop_a_part = 0
    pop_total = 0

    def parse_file_csv(self, filename):
        list_com = []
        header = []
        num = 0
        with open(filename) as f:
            try:
                f_csv = csv.reader(f)
            except IOError:
                print("Error : Can't find this csv file or fail in reading this file ")
            else:
                for row in f_csv:
                    num += 1
                    if num == 7:
                        header = [row[5], row[6], row[2], row[0], row[3], row[4], row[7], row[8], row[9]];
                    elif num > 7:
                        com = Communes(row[5], row[6], row[2], row[0], row[3], row[4], row[7], row[8], row[9])
                        list_com.append(com)
        return list_com, header


def parse_file_csv(filename):
    list_data = []
    header = []
    num = 0
    with open(filename) as f:
        try:
            f_csv = csv.reader(f)
        except IOError:
            print("Error : Can't find this csv file or fail in reading this file ")
        else:
            for row in f_csv:
                num += 1
                if num == 7:
                    header = row
                elif num > 7:
                    list_data.append(row)
    return list_data, header


# Calculer et afficher les populations totales de chaque département et région.
# Les comparer à la main avec les populations indiquées dans les fichiers.

def show_population_total(cursor):
    list_1 = []
    cursor.execute(
        '''SELECT Coderegion, Nomdelaregion, SUM(Populationtotale) AS Sum_pop_region FROM departements GROUP BY Coderegion''')
    for row in cursor:
        list_1.append([row[0], row[1], row[2]])
    for i in list_1:
        cursor.execute('''SELECT Coderegion, Populationtotale FROM regions WHERE Coderegion=?''', (i[0],))
        try:
            num = cursor.fetchone()[1]
        except:
            num = ""
        i.append(num)
    list_1.pop(0)
    print(tabulate(list_1, headers=['region_code', 'region_name', 'pop_total_calculated', 'pop_total_from_T_region']))

    list_2 = []
    cursor.execute( '''SELECT c.Codedepartement, Nomdudepartement, SUM(c.Populationtotale) AS Sum_pop_departe \
                     FROM communes c , departements d WHERE c.Codedepartement =d.Codedepartement GROUP BY d.Codedepartement''')
    for row in cursor:
        list_2.append([row[0], row[1], row[2]])
    for i in list_2:
        cursor.execute('''SELECT Codedepartement, Populationtotale FROM departements WHERE Codedepartement=?''', (i[0],))
        try:
            num = cursor.fetchone()[1]
        except:
            num = ""
        i.append(num)
    print(tabulate(list_2, headers=['dept_code', 'dept_name', 'pop_total_calculated', 'pop_total_from_T_dept']))
    cursor.close()




# Déterminer les communes ayant le même nom et un département différent.
# Afficher le nom de la commune suivi de la liste des n° de départements

def show_communes_with_same_name(cursor):
    cursor.execute(
        '''SELECT Nomdelacommune, Codedepartement FROM communes GROUP BY Nomdelacommune HAVING Count(Codedepartement) > 1  ''')
    list_2 = []
    for row in cursor:
        list_2.append([row[0], row[1]])
    print(tabulate(list_2, headers=['name_commune', 'code_de_departement']))
    cursor.close()


# Ecrire une fonction pour sauvegarder la base dans un fichier XML
# et une autre pour charger la base à partir de ce fichier.
def export_db_as_xml(xml_file, cursor):
    # dir = os.path.dirname(__file__)
    # os.mkdir(dir+"/xml")
    # _file = open((dir+"//xml//regions.xml"), 'w')
    _file = open(xml_file, 'w')
    cursor.execute('''SELECT * FROM regions''')
    rows_1 = cursor.fetchall()
    _file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    _file.write('<root>\n')
    _file.write('<table name="regions">\n')
    for row in rows_1:
        _file.write('  <row>\n')
        _file.write('    <Coderegion>%s</Coderegion>\n' % row[0])
        _file.write('    <Nomdelaregion>%s</Nomdelaregion>\n' % row[1])
        _file.write('    <Nombredarrondissements>%s</Nombredarrondissements>\n' % row[2])
        _file.write('    <Nombredecantons>%s</Nombredecantons>\n' % str(row[3]).replace(" ", ""))
        _file.write('    <Nombredecommunes>%s</Nombredecommunes>\n' % str(row[4]).replace(" ", ""))
        _file.write('    <Populationmunicipale>%s</Populationmunicipale>\n' % str(row[5]).replace(" ", ""))
        _file.write('    <Populationtotale>%s</Populationtotale>\n' % str(row[6]).replace(" ", ""))
        _file.write('  </row>\n')
    _file.write('</table>\n')

    cursor.execute('''SELECT * FROM departements''')
    rows_2 = cursor.fetchall()
    _file.write('<table name="departements">\n')
    for row in rows_2:
        _file.write('  <row>\n')
        _file.write('    <Coderegion>%s</Coderegion>\n' % row[0])
        _file.write('    <Nomdelaregion>%s</Nomdelaregion>\n' % row[1])
        _file.write('    <Codededepartement>%s</Codedepartement>\n' % row[2])
        _file.write('    <Nomdudepartement>%s</Nomdudepartement>\n' % row[3])
        _file.write('    <Nombredarrondissements>%s</Nombredarrondissements>\n' % str(row[4]).replace(" ", ""))
        _file.write('    <Nombredecantons>%s</Nombredecantons>\n' % str(row[5]).replace(" ", ""))
        _file.write('    <Nombredecommunes>%s</Nombredecommunes>\n' % str(row[6]).replace(" ", ""))
        _file.write('    <Populationmunicipale>%s</Populationmunicipale>\n' % str(row[7]).replace(" ", ""))
        _file.write('    <Populationtotale>%s</Populationtotale>\n' % str(row[8]).replace(" ", ""))
        _file.write('  </row>\n')
    _file.write('</table>\n')

    cursor.execute('''SELECT * FROM communes''')
    rows_3 = cursor.fetchall()
    _file.write('<table name="communes">\n')
    for row in rows_3:
        _file.write('  <row>\n')
        _file.write('    <Coderegion>%s</Coderegion>\n' % row[0])
        _file.write('    <Nomdelaregion>%s</Nomdelaregion>\n' % row[1])
        _file.write('    <Codededepartement>%s</Codedepartement>\n' % row[2])
        _file.write('    <Codearrondissement>%s</Codearrondissement>\n' % row[3])
        _file.write('    <Codecantons>%s</Codecantons>\n' % row[4])
        _file.write('    <Codecommune>%s</Codecommune>\n' % row[5])
        _file.write('    <Nomdelacommune>%s</Nomdelacommune>\n' % row[6])
        _file.write('    <Populationmunicipale>%s</Populationmunicipale>\n' % str(row[7]).replace(" ", ""))
        _file.write('    <Populationcompteeapart>%s</Populationcompteeapart>\n' % str(row[8]).replace(" ", ""))
        _file.write('    <Populationtotale>%s</Populationtotale>\n' % str(row[9]).replace(" ", ""))
        _file.write('  </row>\n')
    _file.write('</table>\n')
    _file.write('</root>\n')
    cursor.close()



def import_xml_as_db(xml_file, db):
    cursor = db.cursor()
    tree = ET.parse(xml_file)
    root = tree.getroot()
    for table in root:
        # create table
        table_name = table.attrib
        sql = "CREATE TABLE IF NOT EXISTS " + table_name
        cursor.execute(sql)
        db.commit()
        i = 0
        # read each column
        for col in table:
            # for each column
            d = {}
            for ele in col:
                d[ele.tag] = ele.attrib
                if i == 0:
                    str = "ALTER TABLE " + table_name + " ADD " + ele.tag + ''' VARCHAR(30)'''
                    cursor.execute(str)
                    db.commit()
                    i = i + 1
            # insert into db
            k_str = ""
            val_str = ""
            insert_sql = "INSERT INTO " + table_name + "(" + k_str + ")" + "VALUES" + "(" + val_str + ")"
            len_k_str = 1
            for key in d.keys():
                if len_k_str == len(d):
                    k_str = k_str + key
                    val_str = val_str + "\'" + d[key] + "\'"
                else:
                    k_str = k_str + key + ", "
                    val_str = val_str + "\'" + d[key] + "\'" + ", "
                len_k_str = len_k_str + 1
            cursor.execute(insert_sql)
            db.commit()


if __name__ == '__main__':
    # db = sqlite3.connect('bdd.db')
    # cursor = db.cursor()
    # show_communes_with_same_name(cursor)

    # db to xml
    # export_db_as_xml('db.xml',cursor)
    # db.commit()

    # xml to db
    db = sqlite3.connect('xml_to_db.db')
    import_xml_as_db('xml/regions.xml', db)
    db.commit()
