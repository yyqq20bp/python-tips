from tkinter import *
from tkinter import messagebox
from tkinter.messagebox import showinfo
import tkinter as tk
import hashlib
import csv


class Login:

    def __init__(self):
        self.root = Tk()
        self.root.title("Login")
        self.root.geometry('300x200')
        self.root.resizable(width=False, height=False)

        # username label
        self.username_label = Label(self.root, text='USERNAME', font=('Ariel', 12), width=50, height=0)
        self.username_label.pack()

        # username input
        self.usr = StringVar()
        self.username = Entry(self.root, textvariable=self.usr, font=('Ariel', 12))
        self.username.pack()

        # password label
        self.password_label = Label(self.root, text='PASSWORD', font=('Ariel', 12), width=50, height=0)
        self.password_label.pack()

        # password input
        self.pwd = StringVar()
        self.password = Entry(self.root, textvariable=self.pwd, show='*', font=('Ariel', 12))
        self.password.pack()

        # button submit
        self.btn_submit = Button(self.root, text='Submit', width=25, height=0, command=self.btn_submit_clicked)
        self.btn_submit.pack()

        # button cancel
        self.btn_cancel = Button(self.root, text='Cancel', width=25, height=0, command=self.btn_cancel_clicked)
        self.btn_cancel.pack()

        # button sign in
        self.btn_signin = Button(self.root, text='Sign in', width=25, height=0, command=self.btn_signin_clicked)
        self.btn_signin.pack()

        self.root.mainloop()

    def btn_submit_clicked(self):
        # hash password
        hash_pwd = hashlib.sha256(self.pwd.get().encode()).hexdigest()
        is_existing = False
        pwd_right = False
        with open('info.csv', encoding='utf-8') as f:
            try:
                f_csv = csv.reader(f)
            except IOError:
                print("Error : Can't find this csv file or fail in reading this file ")
            else:
                for line in f_csv:
                    row = line[0].split(';')
                    if self.usr.get().__eq__(row[0]):
                        is_existing = True
                        if hash_pwd.__eq__(row[1]):
                            pwd_right = True
                            break
                if not is_existing:
                    messagebox.showerror(title="Error", message="This account doesn't exist, you could sign in. ")
                else:
                    if not pwd_right:
                        messagebox.showerror(title="Error", message="Your password is not correct.")
                        self.password.delete(0, END)
                    else:
                        messagebox.showerror(title="Success", message="Login successfully!")
                        self.username.delete(0, END)
                        self.password.delete(0, END)

    def btn_cancel_clicked(self):
        self.username.delete(0, END)
        self.password.delete(0, END)

    def btn_signin_clicked(self):
        hash_pwd = hashlib.sha256(self.pwd.get().encode()).hexdigest()
        is_existing = False
        with open('info.csv', mode='r', encoding='utf-8') as f:
            try:
                f_csv = csv.reader(f)
            except IOError:
                print("Error : Can't find this csv file or fail in reading this file ")
            else:
                for line in f_csv:
                    row = line[0].split(';')
                    print(row)
                    if self.usr.get().__eq__(row[0]):
                        is_existing = True
                        messagebox.showerror(title="Error",
                                             message="This account already exists, you could NOT sign in. ")
                        self.username.delete(0, END)
                        self.password.delete(0, END)
                        break
            finally:
                f.close()

        with open('info.csv', mode='a+', encoding='utf-8') as af:
            if not is_existing:
                writer = csv.writer(af, delimiter=';')
                writer.writerow([self.usr.get(), hash_pwd])
                messagebox.showerror(title="Success", message="Sign in successfully!")
                self.username.delete(0, END)
                self.password.delete(0, END)


if __name__ == '__main__':
    login = Login()
