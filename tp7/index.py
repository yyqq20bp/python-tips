#!/usr/bin/python3.6
import cgi

form = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")
html = """<!DOCTYPE html>
<head>
<meta charset="UTF-8"/>
<title>Index</title>
</head>
<body>
<p><a href="/login.py">>Login</a></p>
<h1>Index</h1>
""" + str(form.getvalue("name")) + """
<form action="/index.py" method="post">
<input type="text" name="name" value="" placeholder="Your name" />
<input type="submit" name="send" value="Send info to server" />
</form>
</body>
</html>
"""

if form.getvalue("name") is not None:
    with open("data", "a") as file:
        file.write(form.getvalue("name") + "\n")

print(html)
