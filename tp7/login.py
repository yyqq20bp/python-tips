import cgi
import hashlib
import csv

# Name of the file containing the logins / passwords
filename = "users"
form = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")

# Login form
html = """<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>
<p><a href="/index.py">>Index</a></p>
<h1>Login</h1>
<form action="/login.py" method="post">
<input type="text" name="login" placeholder="User" />
<input type="password" name="password" placeholder="Password"/>
<input type="submit" name="submit" value="Login">
</form>
</body>
</html>
"""

# Gets the login and password from the form
login = str(form.getvalue("login"))
password = str(form.getvalue("password"))

# Searches for the information in the file
found = False
correct = False
with open(filename, 'r') as file:
    reader = csv.reader(file, delimiter=";")
    for row in reader:
        if row[0] == login:
            if row[1] == hashlib.sha512(password.encode()).hexdigest():
                correct = True
            found = True

# If login and password are correct,
# the data are displayed
if correct:
    with open("data", "r") as file:
        data = file.read()

    html2 = """<!DOCTYPE html>
    <head>
    <meta charset="UTF-8">
    <title>Login</title>
    </head>
    <body>
    <p>
    Welcome
    """ + login + """
    !
    </p>
    <h2>Data</h2>
    """ + data.replace("\n", "<br/>") + """
    </body>
    </html>
    """
    html += html2

# Else a message is printed
elif login != "None" or password != "None":
    message = ""
    if found:
        message = "Wrong password for \"" + login + "\", please try again"
    else:
        message = "Unknown login \"" + login + "\", please try again"
    html2 = """<!DOCTYPE html>
    <head>
    <meta charset="UTF-8">
    <title>Login</title>
    </head>
    <body>
    <p>
    """ + message + """ 
    </p>
    </body>
    </html>
    """
    html += html2

print(html)
