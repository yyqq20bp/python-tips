from tkinter import *
from tkinter import messagebox
from tkinter.messagebox import showinfo

window = Tk()
window.title("Calculator")

# Label : operation and result
operationLabelValue = ""
operationLabel = Label(window, text=operationLabelValue, anchor=E, padx=5)

# Label : the current input
inputLabelValue = ""
inputLabel = Label(window, text=inputLabelValue, anchor=E, relief=GROOVE, padx=5)

# Tells if "=" is the last pressed key
equalsPressed = False


# Button : "AC"
def clean_all(event):
    operationLabel["text"] = ""
    inputLabel["text"] = ""


# button "C"
def clean_input(event):
    inputLabel["text"] = ""


# click on a number
def num_click(event):
    """"
    Event handler for a click on a number of the calculator
    """
    global equalsPressed

    # If the input label is empty, only one zero is authorized to be displayed,
    # to avoid "0001" to be written for example
    if operationLabel["text"] == "" or not equalsPressed:
        if inputLabel["text"] == "0":
            inputLabel["text"] = event.widget["text"]
        else:
            inputLabel["text"] += event.widget["text"]
        equalsPressed = False

    # Resets the input label if "=" has been clicked
    elif equalsPressed:
        operationLabel["text"] = ""
        inputLabel["text"] = event.widget["text"]
        equalsPressed = False


#
#
# Operator + - * /
def operator_click(event):
    global equalsPressed

    # If no number in input
    if inputLabel["text"] == "":

        # If the operation label is empty, allows to start by "-" or "+"
        if operationLabel["text"] == "" and (event.widget["text"] == "+" or event.widget["text"] == "-"):
            operationLabel["text"] += event.widget["text"]
            equalsPressed = False

        # - The operation ends with a number
        # - The operation ends with a number and an operator and the pressed operator is "+" or "-"
        elif re.match(".*[0-9]$", operationLabel["text"]) \
                or (re.match(".*[0-9](\s|)(\+|\-|\*|/)$", operationLabel["text"])
                    and (event.widget["text"] == "+" or event.widget["text"] == "-")):
            operationLabel["text"] += " " + event.widget["text"]
            equalsPressed = False

    # If number in input
    elif inputLabel["text"] != "" \
            and (re.match(".*([0-9]|\*|/|\-|\+)$", operationLabel["text"]) or operationLabel["text"] == ""):
        operationLabel["text"] += inputLabel["text"] + " " + event.widget["text"]
        inputLabel["text"] = ""
        equalsPressed = False


# Operator =
def equals_click(event):
    global equalsPressed

    # - The operation label is not empty, does not end with an operator
    # - The input label and the operation label are not empty
    if (operationLabel["text"] != "" and not re.match(".*\D$", operationLabel["text"])) \
            or (inputLabel["text"] != "" and operationLabel["text"] != ""):
        try:
            operationLabel["text"] = str(int(eval(operationLabel["text"] + inputLabel["text"])))
            inputLabel["text"] = ""
            equalsPressed = True
        except Exception:
            messagebox.showerror("Error syntax", "Error syntax")

    # Else if the operation label is empty and the input label is not empty
    elif operationLabel["text"] == "" and inputLabel != "":
        operationLabel["text"] = inputLabel["text"]
        inputLabel["text"] = ""
        equalsPressed = True


# Button help
def help_click(event):
    showinfo(message="Infomation may be helpful to you here")


# Button change mode
def change_click(event):
    print('...')


# Button (

# all the inputs
operationLabel.grid(row=1, column=1, columnspan=5, sticky=EW, pady=(10, 5), padx=10)
inputLabel.grid(row=2, column=1, columnspan=5, sticky=EW, pady=(10, 5), padx=10)

# button design
# 123456789
button1 = Button(window, text="1", width=4, borderwidth=1)
button1.grid(row=3, column=1, padx=(10, 0))
button1.bind("<Button-1>", num_click)

button2 = Button(window, text="2", width=4, borderwidth=1)
button2.grid(row=3, column=2)
button2.bind("<Button-1>", num_click)

button3 = Button(window, text="3", width=4, borderwidth=1)
button3.grid(row=3, column=3)
button3.bind("<Button-1>", num_click)

button4 = Button(window, text="4", width=4, borderwidth=1)
button4.grid(row=4, column=1, padx=(10, 0))
button4.bind("<Button-1>", num_click)

button5 = Button(window, text="5", width=4, borderwidth=1)
button5.grid(row=4, column=2)
button5.bind("<Button-1>", num_click)

button6 = Button(window, text="6", width=4, borderwidth=1)
button6.grid(row=4, column=3)
button6.bind("<Button-1>", num_click)

button7 = Button(window, text="7", width=4, borderwidth=1)
button7.grid(row=5, column=1, padx=(10, 0))
button7.bind("<Button-1>", num_click)

button8 = Button(window, text="8", width=4, borderwidth=1)
button8.grid(row=5, column=2)
button8.bind("<Button-1>", num_click)

button9 = Button(window, text="9", width=4, borderwidth=1)
button9.grid(row=5, column=3)
button9.bind("<Button-1>", num_click)

# +  0 -
button_plus = Button(window, text="+", width=3, borderwidth=1)
button_plus.grid(row=6, column=1, padx=(10, 0))
button_plus.bind("<Button-1>", operator_click)

button_0 = Button(window, text="0", width=3, borderwidth=1)
button_0.grid(row=6, column=2)
button_0.bind("<Button-1>", num_click)

button_sub = Button(window, text="-", width=3, borderwidth=1)
button_sub.grid(row=6, column=3)
button_sub.bind("<Button-1>", operator_click)

# * / =
button_div = Button(window, text="/", width=3, borderwidth=1)
button_div.grid(row=7, column=1, padx=(10, 0))
button_div.bind("<Button-1>", operator_click)

button_mul = Button(window, text="*", width=3, borderwidth=1)
button_mul.grid(row=7, column=2)
button_mul.bind("<Button-1>", operator_click)

button_equal = Button(window, text="=", width=3, borderwidth=1)
button_equal.grid(row=7, column=3)
button_equal.bind("<Button-1>", equals_click)

button_c = Button(window, text="C", width=3, borderwidth=1)
button_c.grid(row=8, column=1, padx=(10, 0))
button_c.bind("<Button-1>", clean_input)

button_ac = Button(window, text="AC", width=3, borderwidth=1)
button_ac.grid(row=8, column=2)
button_ac.bind("<Button-1>", clean_all)

# Runs the window
window.mainloop()
