#!/usr/bin/python
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import messagebox


class Calculator:

    def __init__(self):
        self.result = 0
        self.expression = ''
        self.root = Tk()
        self.root.geometry('450x600')
        self.root.title('Calculator')
        self.root.resizable(width=False, height=False)

        # param
        self.calList = []
        self.flag = False
        self.result = 0
        self.result_panel1 = None
        self.result_panel2 = None
        self.format = True

        # menu
        self.menubar = Menu(self.root)
        filemenu = Menu(self.menubar, tearoff=0)
        filemenu.add_command(label='Save', command=self.save)
        filemenu.add_command(label='Quit', command=self.quit)
        self.menubar.add_cascade(label='File', menu=filemenu)
        helpmenu = Menu(self.menubar, tearoff=0)
        helpmenu.add_command(label='About us', command=self.aboutus)
        helpmenu.add_separator()
        self.menubar.add_cascade(label='Help', menu=helpmenu)
        self.root['menu'] = self.menubar

        # input and output area
        self.top_frame = Frame(self.root, width=450, height=200)
        self.top_frame.place(x=0, y=0)
        self.result_panel1 = StringVar()
        self.result_panel2 = StringVar()
        self.result_panel1.set('')
        self.result_panel2.set(0)
        result_label1 = Label(self.top_frame, font=(
            'Arial', 25), bg='#EA9CB7', bd='9', anchor='se', textvariable=self.result_panel1)
        result_label1.place(width=450, height=100)
        result_label2 = Label(self.top_frame, font=(
            'Arial', 30), bg='#D288B1', bd='9', anchor='se', textvariable=self.result_panel2)
        result_label2.place(x=0, y=100, width=450, height=100)

        self.bottom_frame = Frame(self.root, width=450, height=480)
        self.bottom_frame.place(x=0, y=200)

        bottom_ac = Button(self.bottom_frame, text='AC', bd='0', font=(
            'Arial', 20), command=lambda: self.pressAC())
        bottom_ac.place(x=0, y=0, width=90, height=80)

        button_c = Button(self.bottom_frame, text='C', bd='0', font=(
            'Arial', 20), command=lambda: self.pressC())
        button_c.place(x=90, y=0, width=90, height=80)

        button_minus = Button(self.bottom_frame, text='±', bd='0', font=(
            'Arial', 20), command=lambda: self.pressMinus())
        button_minus.place(x=180, y=0, width=90, height=80)

        button_left = Button(self.bottom_frame, text='(', bd='0', font=(
            'Arial', 20), command=lambda: self.pressLeft())
        button_left.place(x=270, y=0, width=90, height=80)

        button_right = Button(self.bottom_frame, text=')', bd='0', font=(
            'Arial', 20), command=lambda: self.pressRight())
        button_right.place(x=360, y=0, width=90, height=80)

        button_1 = Button(self.bottom_frame, text='1', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('1'))
        button_1.place(x=0, y=80, width=90, height=80)

        button_2 = Button(self.bottom_frame, text='2', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('2'))
        button_2.place(x=90, y=80, width=90, height=80)

        button_3 = Button(self.bottom_frame, text='3', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('3'))
        button_3.place(x=180, y=80, width=90, height=80)

        button_power = Button(self.bottom_frame, text='^', bd='0', font=('Arial', 20),
                              command=lambda: self.pressOperation('^'))
        button_power.place(x=270, y=80, width=90, height=80)

        button_remainder = Button(self.bottom_frame, text='%', bd='0', font=('Arial', 20),
                                  command=lambda: self.pressOperation('%'))
        button_remainder.place(x=360, y=80, width=90, height=80)

        button_4 = Button(self.bottom_frame, text='4', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('4'))
        button_4.place(x=0, y=160, width=90, height=80)

        button_5 = Button(self.bottom_frame, text='5', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('5'))
        button_5.place(x=90, y=160, width=90, height=80)

        button_6 = Button(self.bottom_frame, text='6', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('6'))
        button_6.place(x=180, y=160, width=90, height=80)

        button_plus = Button(self.bottom_frame, text='+', bd='0', font=('Arial', 20),
                             command=lambda: self.pressOperation('+'))
        button_plus.place(x=270, y=160, width=90, height=80)

        button_sub = Button(self.bottom_frame, text='-', bd='0', font=('Arial', 20),
                            command=lambda: self.pressOperation('-'))
        button_sub.place(x=360, y=160, width=90, height=80)

        button_7 = Button(self.bottom_frame, text='7', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('7'))
        button_7.place(x=0, y=240, width=90, height=80)

        button_8 = Button(self.bottom_frame, text='8', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('8'))
        button_8.place(x=90, y=240, width=90, height=80)

        button_9 = Button(self.bottom_frame, text='9', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('9'))
        button_9.place(x=180, y=240, width=90, height=80)

        button_mul = Button(self.bottom_frame, text='×', bd='0', font=('Arial', 20),
                            command=lambda: self.pressOperation('*'))
        button_mul.place(x=270, y=240, width=90, height=80)

        button_div = Button(self.bottom_frame, text='÷', bd='0', font=('Arial', 20),
                            command=lambda: self.pressOperation('/'))
        button_div.place(x=360, y=240, width=90, height=80)

        button_0 = Button(self.bottom_frame, text='0', bd='0', font=('Arial', 20),
                          command=lambda: self.pressNum('0'))
        button_0.place(x=90, y=320, width=90, height=80)

        button_point = Button(self.bottom_frame, text='.', bd='0', font=('Arial', 20),
                              command=lambda: self.pressNum('.'))
        button_point.place(x=180, y=320, width=90, height=80)

        button_eq = Button(self.bottom_frame, text='=', bd='0', font=('Arial', 20),
                           command=lambda: self.pressEqual())
        button_eq.place(x=270, y=320, width=180, height=80)

        button_jump = Button(self.bottom_frame, text='More', bd='0', font=('Arial', 20),
                             command=lambda: self.Cover())
        button_jump.place(x=0, y=320, width=90, height=80)

        self.root.mainloop()

    def save(self):
        if self.expression == '':
            content = 'NULL\n\t\t%d\n' % self.result
        else:
            content = '%s\t\t%f\n' % (self.expression, self.result)
        with open('Calculatrice.txt', 'at') as fileObj:
            fileObj.write(content)
        messagebox.showinfo(title='Success',
                            message=' The expression and the result are already saved in the file Calculatrice.txt !')

    def quit(self):
        messagebox.askokcancel(title='quit',
                               message='Do you want to leave the system?')
        sys.exit()

    def aboutus(self):
        messagebox.showinfo(title='About us',
                            message='Supported by Polytech Tours DI-5 JIANG Yaqi')

    def pressAC(self):
        self.calList.clear()
        self.flag = False
        self.result_panel1.set('')
        self.result_panel2.set(0)

    def pressC(self):
        result = self.result_panel2.get()
        result = result[:-1]
        self.calList.clear()
        self.calList.append(result)
        if self.calList[0] == '':
            self.result_panel2.set(0)
        else:
            self.result_panel2.set(''.join(self.calList))

    def pressMinus(self):
        num = self.result_panel2.get()
        if num[0] == '(' and num[-1] == ')' or num[0] == '-':
            if num[1] == '-':
                num = str(num)[2:-1]
            if num[0] == '-':
                num = str(num)[1:]
        elif num[0] != '-':
            num = '(-' + num + ')'
        self.result_panel2.set(num)
        if len(self.calList) > 0:
            self.calList[-1] = num
        if len(self.calList) == 0:
            self.calList.append(num)

    def pressLeft(self):
        self.calList.append('(')
        self.result_panel2.set(''.join(self.calList))

    def pressRight(self):
        self.calList.append(')')
        self.result_panel2.set(''.join(self.calList))

    def pressSin(self):
        self.calList.append('sin()')
        self.result_panel2.set(''.join(self.calList))

    def pressACos(self):
        self.calList.append('cos()')
        self.result_panel2.set(''.join(self.calList))

    def pressTan(self):
        self.calList.append('tan()')
        self.result_panel2.set(''.join(self.calList))

    def pressFractal(self):
        self.calList.clear()
        result = self.result_panel2.get()
        self.calList.append('1' + '/' + result)
        self.result_panel2.set(''.join(self.calList))

    def pressSqrt(self):
        self.calList.clear()
        result = self.result_panel2.get()
        self.calList.append('sqrt({})'.format(result))
        self.result_panel2.set(''.join(self.calList))

    def pressNum(self, num):
        oldNum = self.result_panel2.get()
        if oldNum == '0' and self.flag == False:
            if num == '.':
                num = '0.'
            self.result_panel2.set(num)
        else:
            if self.flag == True and oldNum[0] != '(':
                if len(self.calList) == 1:
                    self.result_panel2.set(num)
                    self.calList.clear()
                    self.calList.append(num)
                else:
                    self.calList.append(num)
                    self.result_panel2.set(''.join(self.calList).
                                           replace('*', '×').
                                           replace('/', '÷'))
                self.flag = False
            else:
                self.flag = False
                if len(self.calList) != 0 and (
                        'sin' in self.calList[-1] or 'cos' in self.calList[-1] or 'tan' in self.calList[-1]):
                    if 'sin' in self.calList[-1]:
                        s = re.findall(r"sin\(([^\)]+)\)",
                                       ''.join(self.calList[-1]))
                        self.calList[-1] = str('sin({})').format(''.join(s) + num)
                        self.result_panel2.set(''.join(self.calList))
                    elif 'cos' in self.calList[-1]:
                        s = re.findall(r"cos\(([^\)]+)\)",
                                       ''.join(self.calList[-1]))
                        self.calList[-1] = str('cos({})').format(''.join(s) + num)
                        self.result_panel2.set(''.join(self.calList))
                    elif 'tan' in self.calList[-1]:
                        s = re.findall(r"tan\(([^\)]+)\)",
                                       ''.join(self.calList[-1]))
                        self.calList[-1] = str('tan({})').format(''.join(s) + num)
                        self.result_panel2.set(''.join(self.calList))
                else:
                    newNum = oldNum + num
                    self.result_panel2.set(newNum)
                    self.calList.clear()
                    self.calList.append(newNum)

    def pressOperation(self, operation):
        num = self.result_panel2.get()
        if num[-1] in '+-÷×^%':
            self.format = False
        if len(num) > 0:
            if num[0] == '(' and len(num) != 1:
                self.calList.clear()
                self.calList.append('(' + num[1:])
            else:
                self.calList.clear()
                self.calList.append(num)

        self.isPressOperation = True
        self.calList.append(operation)
        self.result_panel2.set(
            ''.join(self.calList).replace('/', '÷').replace('*', '×'))

    def pressEqual(self):
        if self.format == False:
            self.format = True
            try:
                raise FormError("Form error")
            except FormError:
                self.result_panel2.set('Operator error')
                self.calList.clear()
                self.result_panel1.set('')
                return
        try:
            if len(self.calList) != 0:
                self.result = round(eval(''.join(self.calList).replace('^', '**').replace('÷', '/').
                                         replace(
                                             '×', '*').replace('sin', 'math.sin')
                                         .replace('cos', 'math.cos')
                                         .replace('tan', 'math.tan')
                                         .replace('sqrt', 'math.sqrt')), 8)
                self.result_panel2.set(self.result)
                self.result_panel1.set(''.join(self.calList))
                self.calList.clear()
                self.calList.append(str(self.result))
                self.flag = True
            else:
                self.result_panel1.set(0)
        except SyntaxError:
            self.result_panel2.set('No operand')
            self.calList.clear()
            self.result_panel1.set('')
        except ZeroDivisionError:
            self.result_panel2.set('Divisor cannot be 0')
            self.calList.clear()
            self.result_panel1.set('')
        except:
            self.result_panel2.set('ERROR')
            self.calList.clear()
            self.result_panel1.set('')

    def Mouse_Press3(self, e):
        global color_list
        color_list = ['#6495ed', '#8b008b', '#00ced1']
        if self.color_index == len(color_list):
            self.color_index = 0
        e.widget['bg'] = color_list[self.color_index]
        self.color_index += 1

    # Mouse wheel event
    def Mouse_on(self, e):
        if e.delta == -120 and self.nums > 0.11:
            self.nums -= 0.1
            self.root.attributes("-alpha", self.nums)
        elif e.delta == 120 and self.nums < 1:
            self.nums += 0.1
            self.root.attributes("-alpha", self.nums)

    def call_fun(self):
        self.bottom_frame.bind_class(
            'Button', '<ButtonPress-3>', self.Mouse_Press3)
        self.root.bind('<MouseWheel>', self.Mouse_on)

    def Cover(self):
        self.root.minsize(height=680, width=450)
        button_jump = Button(self.bottom_frame, text='Normal', bd='0', font=(' Arial', 20),
                             command=lambda: self.Commer())
        button_jump.place(x=0, y=320, width=90, height=80)

        button_sin = Button(self.bottom_frame, text='sin(x)', bd='0', font=(' Arial', 20),
                            command=lambda: self.pressSin())
        button_sin.place(x=0, y=400, width=90, height=80)

        bottom_acos = Button(self.bottom_frame, text='cos(x)', bd='0', font=(' Arial', 20),
                             command=lambda: self.pressACos())
        bottom_acos.place(x=90, y=400, width=90, height=80)

        button_tan = Button(self.bottom_frame, text='tan(x)', bd='0', font=(' Arial', 20),
                            command=lambda: self.pressTan())
        button_tan.place(x=180, y=400, width=90, height=80)

        button_ds = Button(self.bottom_frame, text='1/x', bd='0', font=(' Arial', 20),
                           command=lambda: self.pressFractal())
        button_ds.place(x=270, y=400, width=90, height=80)

        button_sl = Button(self.bottom_frame, text='√x', bd='0', font=(' Arial', 20),
                           command=lambda: self.pressSqrt())
        button_sl.place(x=360, y=400, width=90, height=80)

    def Commer(self):
        self.root.minsize(height=600, width=450)
        button_jump = Button(self.bottom_frame, text='More', bd='0', font=(' Arial', 20),
                             command=lambda: self.Cover())
        button_jump.place(x=0, y=320, width=90, height=80)


class FormError(Exception):
    pass


if __name__ == '__main__':
    cal = Calculator()
