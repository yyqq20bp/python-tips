#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from tkinter import *
from tkinter import messagebox


class Calculatrice:

    def __init__(self):
        self.result = 0
        self.expression = ''

        self.root = Tk()
        self.root.geometry('350x300')
        self.root.title('Calculatrice')
        self.root.resizable(width=False, height=False)

        self.menubar = Menu(self.root)
        filemenu = Menu(self.menubar, tearoff=0)
        filemenu.add_command(label='Enregistrer', command=self.enregistrer)
        filemenu.add_command(label='Quitter', command=self.quitter)
        self.menubar.add_cascade(label='Fichier', menu=filemenu)
        helpmenu = Menu(self.menubar, tearoff=0)
        helpmenu.add_command(label='A propos', command=self.aPropos)
        helpmenu.add_separator()
        helpmenu.add_command(label='Tutorial', command=self.tutorial)
        self.menubar.add_cascade(label='Aide', menu=helpmenu)

        self.root['menu'] = self.menubar

        self.text = Text(self.root, width=40, height=3)
        self.text.place(x=10, y=10)
        self.text.bind('<KeyPress>', lambda e: 'break')

        self.btn_parentheseL = Button(self.root,text='(',fg='black',bg='MintCream',width=3,height=1,command=self.btn_parentheseL_clicked)
        self.btn_parentheseL.place(x=10,y=70)
        self.btn_parentheseR = Button(self.root,text=')',fg='black',bg='MintCream',width=3,height=1,command=self.btn_parentheseR_clicked)
        self.btn_parentheseR.place(x=60,y=70)
        self.btn_puissance = Button(self.root,text='x^y',fg='black',bg='MintCream',width=3,height=1,command=self.btn_puissance_clicked)
        self.btn_puissance.place(x=110,y=70)
        self.btn_tan = Button(self.root,text='tan',fg='black',bg='MintCream',width=3,height=1,command=self.btn_tan_clicked)
        self.btn_tan.place(x=160,y=70)
        self.btn_sin = Button(self.root,text='sin',fg='black',bg='MintCream',width=3,height=1,command=self.btn_sin_clicked)
        self.btn_sin.place(x=210,y=70)
        self.btn_cos = Button(self.root,text='cos',fg='black',bg='MintCream',width=3,height=1,command=self.btn_cos_clicked)
        self.btn_cos.place(x=260,y=70)

        self.btn_7 = Button(self.root, text='7', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_7_clicked)
        self.btn_7.place(x=10, y=120)
        self.btn_8 = Button(self.root, text='8', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_8_clicked)
        self.btn_8.place(x=70, y=120)
        self.btn_9 = Button(self.root, text='9', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_9_clicked)
        self.btn_9.place(x=130, y=120)
        self.btn_C = Button(self.root, text='C', fg='white', bg='MediumVioletRed', width=5, height=1,
                            command=self.btn_C_clicked)
        self.btn_C.place(x=190, y=120)
        self.btn_AC = Button(self.root, text='AC', fg='white', bg='MediumVioletRed', width=5, height=1,
                             command=self.btn_AC_clicked)
        self.btn_AC.place(x=250, y=120)

        self.btn_4 = Button(self.root, text='4', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_4_clicked)
        self.btn_4.place(x=10, y=170)
        self.btn_5 = Button(self.root, text='5', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_5_clicked)
        self.btn_5.place(x=70, y=170)
        self.btn_6 = Button(self.root, text='6', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_6_clicked)
        self.btn_6.place(x=130, y=170)
        self.btn_mul = Button(self.root, text='*', fg='white', bg='Orange', width=5, height=1,
                              command=self.btn_mul_clicked)
        self.btn_mul.place(x=190, y=170)
        self.btn_div = Button(self.root, text='/', fg='white', bg='Orange', width=5, height=1,
                              command=self.btn_div_clicked)
        self.btn_div.place(x=250, y=170)

        self.btn_1 = Button(self.root, text='1', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_1_clicked)
        self.btn_1.place(x=10, y=220)
        self.btn_2 = Button(self.root, text='2', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_2_clicked)
        self.btn_2.place(x=70, y=220)
        self.btn_3 = Button(self.root, text='3', fg='white', bg='DimGray', width=5, height=1,
                            command=self.btn_3_clicked)
        self.btn_3.place(x=130, y=220)
        self.btn_add = Button(self.root, text='+', fg='white', bg='Orange', width=5, height=1,
                              command=self.btn_add_clicked)
        self.btn_add.place(x=190, y=220)
        self.btn_sub = Button(self.root, text='-', fg='white', bg='Orange', width=5, height=1,
                              command=self.btn_sub_clicked)
        self.btn_sub.place(x=250, y=220)

        self.btn_0 = Button(self.root, text='0', fg='white', bg='DimGray', width=14, height=1,
                            command=self.btn_0_clicked)
        self.btn_0.place(x=10, y=270)
        self.btn_point = Button(self.root, text='.', fg='white', bg='DimGray', width=5, height=1,
                                command=self.btn_point_clicked)
        self.btn_point.place(x=130, y=270)
        self.btn_answer = Button(self.root, text='Ans', fg='white', bg='LightSkyBlue', width=5, height=1,
                                 command=self.btn_answer_clicked)
        self.btn_answer.place(x=190, y=270)
        self.btn_equal = Button(self.root, text='=', fg='white', bg='Orange', width=5, height=1,
                                command=self.btn_equal_clicked)
        self.btn_equal.place(x=250, y=270)

        self.root.mainloop()

    def enregistrer(self):
        if self.expression == '':
            content = 'NULL\n\t\t%d\n' % self.result
        else:
            content = '%s\t\t%f\n' % (self.expression, self.result)
        with open('Calculatrice.txt', 'at') as fileObj:
            fileObj.write(content)
        messagebox.showinfo(title='Succès',
                            message='L\'expression et le résultat sont déjà enregistrés dans le fichier \'Calculatrice.txt\'!')

    def quitter(self):
        messagebox.askokcancel(title='Quitter',
                               message='Vous voulez quitter le système?')
        sys.exit()

    def aPropos(self):
        messagebox.showinfo(title='A propos',
                            message='Développeurs: BI Peng & ZENG Kai\nVersion: 1.0.0')

    def tutorial(self):
        messagebox.showinfo(title='Tutorial',
                            message='1) Le bouton \'Ans\' est pour enregistrer le résultat précédent;\n'
                                    '2) La puissance : le bouton \'0-9\' --> le bouton \'x^y\' --> le bouton \'0-9\';\n'
                                    '3) Les trigos : le boution \'tan\' ou \'sin\' ou \'cos\' --> le bouton \'0-9\'!')

    def btn_0_clicked(self):
        self.text.insert(END, '0')

    def btn_1_clicked(self):
        self.text.insert(END, '1')

    def btn_2_clicked(self):
        self.text.insert(END, '2')

    def btn_3_clicked(self):
        self.text.insert(END, '3')

    def btn_4_clicked(self):
        self.text.insert(END, '4')

    def btn_5_clicked(self):
        self.text.insert(END, '5')

    def btn_6_clicked(self):
        self.text.insert(END, '6')

    def btn_7_clicked(self):
        self.text.insert(END, '7')

    def btn_8_clicked(self):
        self.text.insert(END, '8')

    def btn_9_clicked(self):
        self.text.insert(END, '9')

    def btn_point_clicked(self):
        self.text.insert(END, '.')

    def btn_add_clicked(self):
        self.text.insert(END, '+')

    def btn_sub_clicked(self):
        self.text.insert(END, '-')

    def btn_mul_clicked(self):
        self.text.insert(END, '*')

    def btn_div_clicked(self):
        self.text.insert(END, '/')

    def btn_C_clicked(self):
        self.text_content = self.text.get(0.0, END)
        self.text.delete(0.0, END)
        self.text.insert(END, self.text_content[:-2])

    def btn_AC_clicked(self):
        self.text.delete(0.0, END)

    def btn_parentheseL_clicked(self):
        self.text.insert(END, '(')

    def btn_parentheseR_clicked(self):
        self.text.insert(END, ')')

    def btn_puissance_clicked(self):
        self.text.insert(END, '^')

    def btn_tan_clicked(self):
        self.text.insert(END, 'tan')

    def btn_sin_clicked(self):
        self.text.insert(END, 'sin')

    def btn_cos_clicked(self):
        self.text.insert(END, 'cos')

    # Verifier si tous les caractères sont numériques dans la premiere sous-expression divisée par expression.split()
    def isNumericBefore(self, subExp1):
        i = 1
        way = False
        while i <= len(subExp1):
            if subExp1[-i] < '0' or subExp1[-i] > '9':  # A partir de le dernier caractère, s'il n'est pas numerique
                if subExp1[-i] != '.':  # et s'il n'est pas le point
                    way = True
                    break
            i += 1
        return way, i

    # Verifier si tous les caractères sont numériques dans la deuxieme sous-expression divisée par expression.split()
    def isNumericAfter(self, subExp2):
        i = 0
        way = False
        while i <= len(subExp2):
            if subExp2[i] < '0' or subExp2[i] > '9':  # A partir de le dernier caractère, s'il n'est pas numerique
                if subExp2[i] != '.':  # et s'il n'est pas le point
                    way = True
                    break
            i += 1
        return way, i

    def btn_equal_clicked(self):
        self.expression = self.text.get(0.0, END)

        # s'il y a la puissance dans l'expression
        if self.expression.find('^') != -1:
            subExp1 = self.expression.split('^')[0]
            subExp2 = self.expression.split('^')[1]

            if (len(subExp1) >= 1 and subExp1[-1] >= '0' and subExp1[-1] <= '9'):
                way, i = self.isNumericBefore(subExp1)
                if way == True:
                    self.expression = subExp1[:-i + 1] + 'pow(' + subExp1[-i + 1:] + ','
                else:
                    self.expression = 'pow(' + subExp1[:] + ','

            if (len(subExp2) >= 1 and subExp2[0] >= '0' and subExp2[0] <= '9'):
                way, i = self.isNumericAfter(subExp2)
                if way == True:
                    self.expression += subExp2[:i] + ')' + subExp2[i:]
                else:
                    self.expression += subExp2[:] + ')'

        # s'il y a la trigo dans l'expression
        if self.expression.find('tan') != -1:
            self.trigo('tan')
        if self.expression.find('sin') != -1:
            self.trigo('sin')
        if self.expression.find('cos') != -1:
            self.trigo('cos')

        self.result = eval(self.expression)
        self.text.insert(END, '\n = ' + str(self.result))

    # transformer les expressions de trigo
    def trigo(self, typeTrigo):
        subExp1 = self.expression.split(typeTrigo)[0]
        subExp2 = self.expression.split(typeTrigo)[1]

        self.expression = subExp1 + 'math.' + typeTrigo + '('
        if (len(subExp2) >= 1 and subExp2[0] >= '0' and subExp2[0] <= '9'):
            way, i = self.isNumericAfter(subExp2)
            if way == True:
                self.expression += subExp2[:i] + ')' + subExp2[i:]
            else:
                self.expression += subExp2[:] + ')'

    def btn_answer_clicked(self):
        self.text.insert(END, self.result)


if __name__ == '__main__':
    calcul = Calculatrice()
