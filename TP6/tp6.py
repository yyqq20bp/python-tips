import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pylab,pyplot
from scipy import optimize, ndimage
import os
import cv2
from scipy.fftpack import fft2,ifft2

# 1. Créer un tableau de dimension 3 avec un shape de (4, 3, 2) remplit avec des nombres aléatoires.
# Vous afficherez les attributs du tableau : ndim, shape, size, dtype, itemsize, data.
def create_3D_shape():
    x = np.empty([4, 3, 2], dtype=int)
    print(x)
    print("ndim :", x.ndim)
    print("shape :", x.shape)
    print("size :", x.size)
    print("dtype :", x.dtype)
    print("itemsize :", x.itemsize)
    print("date : ", x.data)


# 2. Créer 2 matrices 3x3 initialisées avec les entiers de 0 à 8 pour la 1e et de 2 à 10 pour la 2e puis
# calculer le produit des 2 (différence entre * et dot). Transposer une matrice.
def matrix_cal():
    n = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8]])
    m = np.array([[2, 3, 4], [5, 6, 7], [8, 9, 10]])
    print("n*m = ", n * m)
    print("np.dot(n,m) = ", np.dot(n, m))
    x = (n * m).T
    print("T(n*m) = ", x)
    return x


# 3. Calculer le déterminant et l’inverse d’une matrice. Résoudre un système d’équations linéaires.
def cal_lineaire(x):
    determ = np.linalg.det(x)
    print("le déterminant : ", determ)
    inverse = np.linalg.inv(x)
    print("l’inverse : ", inverse)
    # Calculer les valeurs et vecteurs propres d’une matrice.
    res = np.linalg.eig(x)
    print("les valeurs et vecteurs propres : ", res)
    return determ, inverse, res


# 4. Approcher un ensemble de points par une courbe (optimize.curve_fit ou interpolate.interp1d).
def fmax(x, a, b, c):
    return a * np.sin(x * np.pi / 6 + b) + c


def draw_curve_fitting():
    x = np.arange(1, 13, 1)
    x1 = np.arange(1, 13, 0.1)
    ymax = np.array([17, 19, 21, 28, 33, 38, 37, 37, 31, 23, 19, 18])
    fita, fitb = optimize.curve_fit(fmax, x, ymax, [1, 1, 1])
    print(fita)
    plt.plot(x, ymax)
    plt.plot(x1, fmax(x1, fita[0], fita[1], fita[2]))
    plt.show()


# 5. Lire une image jpeg et afficher l’image originale et réduite en taille.
def modify_jpeg_size(filename):
    pic = os.getcwd()+'/TP6/'+filename
    image_1 = pyplot.imread(pic)
    fig = plt.figure()
    print("Original picture size : ", image_1.shape)
    fig.add_subplot(1, 2, 1)
    plt.imshow(image_1)
    image_2 = cv2.resize(image_1, dsize=(160, 90), interpolation=cv2.INTER_CUBIC)
    print("Modified picture size : ", image_2.shape)
    fig.add_subplot(1, 2, 2)
    plt.imshow(image_2)
    plt.show()

# 6. Explorer d’autres fonctionnalités de scipy  ： Picture noise reduction
def other_functions_scipy(filename):
    pic = os.getcwd()+'/TP6/'+filename
    img1 = pyplot.imread(pic)
    plt.subplot(2,2,1)
    plt.title('origin image')
    plt.imshow(img1)
    # Rotate image
    plt.subplot(2, 2, 2)
    plt.title('Rotate the picture')
    img2 = ndimage.rotate(img1, 45)
    plt.imshow(img2)
    # Image filtering
    plt.subplot(2, 2, 3)
    plt.title('Image filtering')
    img3 = ndimage.gaussian_filter(img1, sigma=3)
    plt.imshow(img3)
    # Edge detection
    plt.subplot(2, 2, 4)
    plt.title('Edge detection')
    img4 = np.zeros((256, 256))
    img4[64:-64, 64:-64] = 1
    img4[90:-90, 90:-90] = 2
    img4 = ndimage.gaussian_filter(img4, 8)
    plt.imshow(img4)
    plt.show()


if __name__ == '__main__':
    other_functions_scipy("test.jpeg")
