from matplotlib import pyplot as plt
from random import randint, random

from matplotlib.ticker import MultipleLocator
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


# list  of 100 numbers in random [0,100]
list_ran = []
list_ran_2 = []
list_ran_3 = []
list_ind = []
for _ in range(20):
    list_ind.append(_)
    list_ran.append(randint(0, 100))
    list_ran_2.append(randint(0, 100))
    list_ran_3.append(randint(0, 100))

print("GÉNÉRER DES NOMBRES ALÉATOIRES : ")
print(list_ran)
print(list_ran_2)
print(list_ran_3)

# Display the curve of this data in a matplotlib window
plt.grid(True)
plt.title('Random numbers\' line chart')
plt.subplot(211)
# plt.plot(list_ind, list_ran, "b--", linewidth=1)
# plt.plot(list_ind, list_ran_2, "b--", linewidth=1)
# plt.plot(list_ind, list_ran_3, "b--", linewidth=1)
plt.plot(list_ind, list_ran, "b--", linewidth=1)
plt.plot(list_ind, list_ran_2, "g", linewidth=0.8, marker="*")
plt.plot(list_ind, list_ran_3, "r", linewidth=1, marker="+")
x_major_locator = MultipleLocator(1)
y_major_locator = MultipleLocator(20)
ax = plt.gca()
ax.xaxis.set_major_locator(x_major_locator)
ax.yaxis.set_major_locator(y_major_locator)
plt.xlim(0, 19)
plt.ylim(0, 100)
plt.xlabel('Index')
plt.ylabel('Value')
plt.legend()

# histogrammes
plt.subplot(311)
plt.title('Histrogramme chart')
plt.hist(list_ran, 20, density=1, facecolor='r', alpha=0.5)

plt.subplot(312)
plt.hist(list_ran_2, 20, density=1, facecolor='c', alpha=0.5)

plt.subplot(313)
plt.hist(list_ran_3, 20, density=1, facecolor='b', alpha=0.5)

x_major_locator_h = MultipleLocator(1)
y_major_locator_h = MultipleLocator(20)
ax_h = plt.gca()
ax_h.xaxis.set_major_locator(x_major_locator_h)
ax_h.yaxis.set_major_locator(y_major_locator_h)
plt.xlim(0, 19)
plt.ylim(0, 100)
plt.xlabel('Index')
plt.ylabel('Value')
plt.legend()

# 2D to 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = y = np.arange(-3.0, 3.0, 0.05)
X, Y = np.meshgrid(x, y)
R = np.sqrt(X**2 + Y**2)
Z = np.sin(R)
ax.plot_surface(X, Y, Z)
ax.set_xlabel('X axe')
ax.set_ylabel('Y axe')
ax.set_zlabel('Z axe')


plt.show()



