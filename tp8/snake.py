import pygame
from pygame.locals import *
import random
import time, sys



class SnakeNibble:
    """Move the snake. When the snake eats fruit, the body will grow larger.\
     The game ends when it hits a wall or hits itself."""

    def __init__(self, width=400, edge=20):
        self.width = self.height = 400  # screen size
        self.edge = edge  # grid size
        self.ball = pygame.Surface((edge, edge))  # Surface object

    def makeSnake(self):
        '''Create teh snake, turn to right'''
        self.ball.fill((220, 20, 60))  # red
        ballrect = self.ball.get_rect()  # get rect object
        a = self.edge * 5
        snake = []
        for i in range(3):
            ballrect2 = ballrect.copy()
            ballrect2.left += a
            ballrect2.top = 100
            a -= self.edge
            snake.append(ballrect2)
        return snake

    def move(self, snake, direct):
        """Move in the direction and return the snake's tail before the update"""
        for i in range(len(snake)):
            if i == 0:
                rect = snake[0].copy()
                temp = self._get_nextRect(rect, direct)
        snake.insert(0, temp)
        endPop = snake.pop()
        return endPop

    def strike(self, snake, food):
        '''Snake impact detection
        ：param snake：Snake Rect Object Sequence
        ：param food：Rect object for food
        ：return ：Hit the wall, return 0 if you hit yourself, the game is over, \
        return 1 if you eat fruit, return 3 without collision \
        '''
        # Hit the wall
        if snake[0].left < 0 or snake[0].right > self.width: return 0
        if snake[0].top < 0 or snake[0].bottom > self.height: return 0
        # Hit itself
        for sr in snake[1:]:
            if sr.colliderect(snake[0]):
                return 0
        # Eat
        if food.colliderect(snake[0]):
            return 1
        return 2

    def _get_nextRect(self, rect, direct):
        """Snake gets new head position"""
        if direct == 0:
            rect.top += self.edge  # to down
        elif direct == 1:
            rect.top -= self.edge  # to up
        elif direct == 2:
            rect.left += self.edge  # to right
        elif direct == 3:
            rect.left -= self.edge  # to left
        return rect


class Food:
    """Randomly generated food"""

    def __init__(self, width=400, edge=20):
        self.width = self.height = 400
        self.edge = edge
        self.food = pygame.Surface((self.edge, self.edge))  # surface object

    def get_food(self):
        """Generate food rect object"""
        self.food.fill((0, 0, 255))  # blue
        fr = self.food.get_rect()
        return fr

    def get_foodpos(self, food, snake):
        '''Get new location for food'''
        while True:
            food.left, food.top = random.randrange(self.edge, self.height, self.edge), random.randrange(self.edge,
                                                                                                        self.height,
                                                                                                        self.edge)  # update fr location
            foodpos = True
            for sn in snake:  # Food update location cannot be inside the snake's body
                if food.colliderect(sn):
                    foodpos = False
                    break
            if foodpos == True:
                break
        if foodpos == True:  # Have location
            return True


class Background:
    def __init__(self, width=400, edge=20):
        self.width = self.height = 400
        self.edge = edge
        self.scoref = pygame.font.SysFont('Arial', 30)  # font

    def drawGrid(self, surface):
        # Draw full screen grid lines
        rows = self.width // self.edge
        sizeBtwn = self.edge
        x, y = 0, 0
        for _ in range(rows):
            x += sizeBtwn
            y += sizeBtwn
            pygame.draw.line(surface, (219, 112, 147), (x, 0), (x, self.width))
            pygame.draw.line(surface, (219, 112, 147), (0, y), (self.width, y))


def main(best):
    """Initialize and run in a loop until there is a return"""
    # 初始化
    pygame.init()
    direct = 2  # Initial direction Right. Up 1, 0 down, 3 left, 2 right
    validDirect = direct  # Effective speed
    edge = 20  # Grid size
    black = 0, 0, 0  # background color
    width, height = 400, 400  # Window size
    screen = pygame.display.set_mode((width, height))  # Initialize a window
    pygame.display.set_caption('Snake')

    # Prepare obejct
    # Snake
    s = SnakeNibble(width, edge)  # snake size = grid size
    snake = s.makeSnake()
    # food
    f = Food(width, edge)  # a food size = a grid size
    foodr = f.get_food()
    f.get_foodpos(foodr, snake)
    # grid
    b = Background(width, edge)

    c = 0  # counter
    dt = 0  # timer
    score = 0  # note
    going = True  # situation
    endPop = None  # snake tail
    interval = 300
    clock = pygame.time.Clock()
    while going:
        lastt = clock.tick(60)  # Frame rate 60
        dt += lastt  # total time
        c += 1
        print('循环次数 %d, 前一次的时间 %d，目前总时间 %d 单位毫秒' % (c, lastt, dt))  # game frame rate
        # 0 Response to events such as keyboard presses
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_UP and validDirect != 0:
                    direct = 1  # 1: up 0 : down
                elif event.key == K_DOWN and validDirect != 1:
                    direct = 0  # 1: up 0 : down
                elif event.key == K_LEFT and validDirect != 2:
                    direct = 3  # 3: left 2 :right
                elif event.key == K_RIGHT and validDirect != 3:
                    direct = 2

                # bgc : black
        screen.fill(black)
        # is moving
        if dt > interval:  # moving time
            validDirect = direct
            dt = 0  # Initialization time
            endPop = s.move(snake, direct)
        # 1.1 draw snake
        for i in snake:
            screen.blit(s.ball, i)

        # 2 draw line of grid
        b.drawGrid(screen)   # 3.1 update note
        scoret = b.scoref.render(str(score), True, (255, 255, 255))  # Real-time scoring
        screen.blit(scoret, (0, 0))
        scoret2 = b.scoref.render('best:' + str(best), True, (255, 255, 255))  # best score
        screen.blit(scoret2, (width - 6 * edge, 0))
        # 3.2 food
        screen.blit(f.food, foodr)
        # is hit
        clli = s.strike(snake, foodr)
        if clli == 0:  # hit the wall or itself
            going = False
        elif clli == 1:  # eat
            snake.append(endPop)  # add tail
            score += 1
            if not f.get_foodpos(foodr, snake): going = False  # 生成 food 新位置, 如果占满全屏，则退出
        # update screen
        pygame.display.flip()  # show image

    # score history
    if score > best:
        return score
    else:
        return best


if __name__ == '__main__':
    best = 0  # Best score for this game
    while True:
        best = main(best)
        time.sleep(1)

