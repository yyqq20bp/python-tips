#!/usr/bin/python
# -*- coding:utf-8 -*-
# ***************************************************************************************************************
# MULTI-PROCESS
# ***************************************************************************************************************
# from multiprocessing import Pool
# import os, time
#
#
# def calcul_long_multi_processing(idProcessing):
#     print('Run process of \'calcul_long\' -%s (%s)...' % (idProcessing, os.getpid()))
#     start = time.time()
#     n = 1E7
#     while n > 0:
#         n -= 1
#     end = time.time()
#     print('Process-%s done! It runs %0.2f s.' % (idProcessing, (end - start)))
#
# if __name__=='__main__':
#     print('Parent process %s.' % os.getpid())
#     p = Pool()
#     for i in range(4):
#         p.apply_async(calcul_long_multi_processing,(i,))
#     print('Waiting for all the sub-processes to be done ...')
#     p.close()   # process pool doesn't create new process any more
#     p.join()    # wait for that all sub-processes finish executing
#     print('All the sub-processes are finished.')
# ***************************************************************************************************************

# !/usr/bin/python
# -*- coding: UTF-8 -*-

import threading
import time

exitFlag = 0


def calcul_long(threadName, start):
    n = 1E7
    while n > 0:
        n -= 1
    end = time.time()
    print("%s used : %0.2f s" % (threadName, (end-start)))

class myThread(threading.Thread):  # Inherit from parent class : threading.Thread
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter

    def run(self):  # 把要执行的代码写到run函数里面 线程在创建后会直接运行run函数
        start = time.time()
        print(self.name + " Starting " + ": %s" % time.ctime(time.time()))
        calcul_long(self.name, start)
        print(self.name + "Exiting " + ": %s" % time.ctime(time.time()))

if __name__=='__main__':
    # create new thread
    thread1 = myThread(1, "Thread-1", 1)
    thread2 = myThread(2, "Thread-2", 2)
    thread3 = myThread(3, "Thread-3", 3)

    # start thread
    thread1.start()
    thread2.start()
    thread3.start()

    print("\nExiting Main Thread")
