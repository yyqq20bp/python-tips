#!/usr/bin/python
# -*- coding:utf-8 -*-
from xml.dom.minidom import parse
import xml.dom.minidom
from PIL import Image
import random
import threading


class Fourmi:
    def __init__(self):
        self.couleurDeposee=[]*3
        self.couleurSuivie=[]*3
        self.probabiliteMouvement=[]*3
        self.typeMouvement=''
        self.probabiliteSuivi=0
        self.x = 0
        self.y = 0
        self.headTo = 'droite'

    def setX(self,x):
        self.x = x

    def getX(self):
        return self.x


    def setY(self, y):
        self.y = y

    def getY(self):
        return self.y

    def setHeadTo(self,teteVers):
        self.headTo = teteVers

    def getHeadTo(self):
        return self.headTo
        
    def setCouleurDeposee(self,Cr,Cv,Cb):
        self.couleurDeposee.append(Cr)
        self.couleurDeposee.append(Cv)
        self.couleurDeposee.append(Cb)

    def getCouleurDeposee(self):
        return self.couleurDeposee

    def setProbabiliteMouvement(self,Pg,Pd,Pt):
        self.probabiliteMouvement.append(Pg)
        self.probabiliteMouvement.append(Pd)
        self.probabiliteMouvement.append(Pt)

    def getProbabiliteMouvement(self):
        return self.probabiliteMouvement

    def setCouleurSuivie(self,Sr,Sv,Sb):
        self.couleurSuivie.append(Sr)
        self.couleurSuivie.append(Sv)
        self.couleurSuivie.append(Sb)

    def getCouleurSuivie(self):
        return self.couleurSuivie

    def setTypeMouvement(self,D):
        self.typeMouvement = D

    def getTypeMouvement(self):
        return self.typeMouvement


    def setProbabiliteSuivi(self,Ps):
        self.probabiliteSuivi = Ps

    def getProbabiliteSuivi(self):
        return self.probabiliteSuivi


def readFourmisFromXml():
    DOMTree = xml.dom.minidom.parse('fourmi.xml')
    peinture = DOMTree.documentElement
    fourmis = peinture.getElementsByTagName('Fourmi')
    return fourmis

def readToileFromXml():
    DOMTree = xml.dom.minidom.parse('fourmi.xml')
    peinture = DOMTree.documentElement
    toile = peinture.getElementsByTagName('Toile')
    return toile

def initializeFourmis(listFourmis):
    fourmis = readFourmisFromXml()
    i = 0
    for fourmi in fourmis:
        coulDepo = fourmi.getElementsByTagName('CouleurDeposee')[0]
        Cr = int(coulDepo.childNodes[0].data.split(",")[0])
        Cv = int(coulDepo.childNodes[0].data.split(",")[1])
        Cb = int(coulDepo.childNodes[0].data.split(",")[2])
        listFourmis[i].setCouleurDeposee(Cr, Cv, Cb)

        coulSuiv = fourmi.getElementsByTagName('CouleurSuivie')[0]
        Sr = int(coulSuiv.childNodes[0].data.split(",")[0])
        Sv = int(coulSuiv.childNodes[0].data.split(",")[1])
        Sb = int(coulSuiv.childNodes[0].data.split(",")[2])
        listFourmis[i].setCouleurSuivie(Sr,Sv,Sb)

        probMouv = fourmi.getElementsByTagName('ProbabiliteMouvement')[0]
        Pg = float(probMouv.childNodes[0].data.split(",")[0])
        Pd = float(probMouv.childNodes[0].data.split(",")[1])
        Pt = float(probMouv.childNodes[0].data.split(",")[2])
        listFourmis[i].setProbabiliteMouvement(Pg,Pd,Pt)

        typeMouv = fourmi.getElementsByTagName('TypeMouvement')[0]
        D = typeMouv.childNodes[0].data
        listFourmis[i].setTypeMouvement(D)

        probSuiv = fourmi.getElementsByTagName('ProbabiliteSuivi')[0]
        Ps = float(probSuiv.childNodes[0].data)
        listFourmis[i].setProbabiliteSuivi(Ps)

        xDepart = random.randint(0, int(taille.split('*')[0]))
        yDepart = random.randint(0, int(taille.split('*')[1]))
        listFourmis[i].setX(xDepart)
        listFourmis[i].setY(yDepart)

        i += 1

def random_pick():
    x  = random.randint(0, 2)
    if x == 0 :
        return 'g'
    elif x == 1 :
        return  'd'
    else:
        return 't'



def tourner(fourmi,x,y,taille):
    Sr = fourmi.getCouleurSuivie()[0]
    Sv = fourmi.getCouleurSuivie()[1]
    Sb = fourmi.getCouleurSuivie()[2]

    x1 = x + 1
    if x == int(taille.split('*')[0])-1:
        x1 = 0

    x0 = x - 1
    if x == 0:
        x0 = int(taille.split('*')[0])-1

    y1 = y + 1
    if y == int(taille.split('*')[1])-1:
        y1 = 0

    y0 = y - 1
    if y == 0:
        y0 = int(taille.split('*')[1])-1
    Pg = fourmi.getProbabiliteMouvement()[0]
    Pd = fourmi.getProbabiliteMouvement()[1]
    Pt = fourmi.getProbabiliteMouvement()[2]

    if fourmi.getTypeMouvement() == 'Dd':
        if fourmi.getHeadTo() == 'droite':
            if (Sr, Sv, Sb) == im.getpixel((x1, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setHeadTo('droite')
            elif (Sr, Sv, Sb) == im.getpixel((x, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y0)
                fourmi.setHeadTo('haut')
            elif (Sr, Sv, Sb) == im.getpixel((x, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y1)
                fourmi.setHeadTo('bas')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setY(y0)
                    fourmi.setHeadTo('haut')
                elif direction == 'd':
                    fourmi.setY(y1)
                    fourmi.setHeadTo('bas')
                else:
                    fourmi.setX(x1)
                    fourmi.setHeadTo('droite')
        elif fourmi.getHeadTo() == 'haut':
            if (Sr, Sv, Sb) == im.getpixel((x, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y0)
                fourmi.setHeadTo('haut')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setHeadTo('gauche')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setHeadTo('droite')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x0)
                    fourmi.setHeadTo('gauche')
                elif direction == 'd':
                    fourmi.setX(x1)
                    fourmi.setHeadTo('droite')
                else:
                    fourmi.setY(y0)
                    fourmi.setHeadTo('haut')
        elif fourmi.getHeadTo() == 'gauche':
            if (Sr, Sv, Sb) == im.getpixel((x0, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setHeadTo('gauche')
            elif (Sr, Sv, Sb) == im.getpixel((x, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y1)
                fourmi.setHeadTo('bas')
            elif (Sr, Sv, Sb) == im.getpixel((x, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y0)
                fourmi.setHeadTo('haut')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setY(y1)
                    fourmi.setHeadTo('bas')
                elif direction == 'd':
                    fourmi.setY(y0)
                    fourmi.setHeadTo('haut')
                else:
                    fourmi.setX(x0)
                    fourmi.setHeadTo('gauche')
        else:
            if (Sr, Sv, Sb) == im.getpixel((x, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y1)
                fourmi.setHeadTo('bas')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setHeadTo('droite')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setHeadTo('gauche')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x1)
                    fourmi.setHeadTo('droite')
                elif direction == 'd':
                    fourmi.setX(x0)
                    fourmi.setHeadTo('gauche')
                else:
                    fourmi.setY(y1)
                    fourmi.setHeadTo('bas')

    elif fourmi.getTypeMouvement() == 'Do':
        if fourmi.getHeadTo() == 'droite':
            if (Sr, Sv, Sb) == im.getpixel((x1, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setHeadTo('droite')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setY(y0)
                fourmi.setHeadTo('droite-haut')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setY(y1)
                fourmi.setHeadTo('droite-bas')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x1)
                    fourmi.setY(y0)
                    fourmi.setHeadTo('droite-haut')
                elif direction == 'd':
                    fourmi.setX(x1)
                    fourmi.setY(y1)
                    fourmi.setHeadTo('droite-bas')
                else:
                    fourmi.setX(x1)
                    fourmi.setHeadTo('droite')
        elif fourmi.getHeadTo() == 'droite-haut':
            if (Sr, Sv, Sb) == im.getpixel((x1, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setY(y0)
                fourmi.setHeadTo('droite-haut')
            elif (Sr, Sv, Sb) == im.getpixel((x, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y0)
                fourmi.setHeadTo('haut')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setHeadTo('droite')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setY(y0)
                    fourmi.setHeadTo('haut')
                elif direction == 'd':
                    fourmi.setX(x1)
                    fourmi.setHeadTo('droite')
                else:
                    fourmi.setX(x1)
                    fourmi.setY(y0)
                    fourmi.setHeadTo('droite-haut')
        elif fourmi.getHeadTo() == 'haut':
            if (Sr, Sv, Sb) == im.getpixel((x, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y0)
                fourmi.setHeadTo('haut')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setY(y0)
                fourmi.setHeadTo('gauche-haut')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setY(y0)
                fourmi.setHeadTo('droite-haut')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x0)
                    fourmi.setY(y0)
                    fourmi.setHeadTo('gauche-haut')
                elif direction == 'd':
                    fourmi.setX(x1)
                    fourmi.setY(y0)
                    fourmi.setHeadTo('droite-haut')
                else:
                    fourmi.setY(y0)
                    fourmi.setHeadTo('haut')
        elif fourmi.getHeadTo() == 'gauche-haut':
            if (Sr, Sv, Sb) == im.getpixel((x0, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setY(y0)
                fourmi.setHeadTo('gauche-haut')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setHeadTo('gauche')
            elif (Sr, Sv, Sb) == im.getpixel((x, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y0)
                fourmi.setHeadTo('haut')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x0)
                    fourmi.setHeadTo('gauche')
                elif direction == 'd':
                    fourmi.setY(y0)
                    fourmi.setHeadTo('haut')
                else:
                    fourmi.setX(x0)
                    fourmi.setY(y0)
                    fourmi.setHeadTo('gauche-haut')
        elif fourmi.getHeadTo() == 'gauche':
            if (Sr, Sv, Sb) == im.getpixel((x0, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setHeadTo('gauche')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setY(y1)
                fourmi.setHeadTo('gauche-bas')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y0)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setY(y0)
                fourmi.setHeadTo('gauche-haut')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x0)
                    fourmi.setY(y1)
                    fourmi.setHeadTo('gauche-bas')
                elif direction == 'd':
                    fourmi.setX(x0)
                    fourmi.setY(y0)
                    fourmi.setHeadTo('gauche-haut')
                else:
                    fourmi.setX(x0)
                    fourmi.setHeadTo('gauche')
        elif fourmi.getHeadTo() == 'gauche-bas':
            if (Sr, Sv, Sb) == im.getpixel((x0, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setY(y1)
                fourmi.setHeadTo('gauche-bas')
            elif (Sr, Sv, Sb) == im.getpixel((x, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y1)
                fourmi.setHeadTo('bas')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setHeadTo('gauche')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setY(y1)
                    fourmi.setHeadTo('bas')
                elif direction == 'd':
                    fourmi.setX(x0)
                    fourmi.setHeadTo('gauche')
                else:
                    fourmi.setX(x0)
                    fourmi.setY(y1)
                    fourmi.setHeadTo('gauche-bas')
        elif fourmi.getHeadTo() == 'bas':
            if (Sr, Sv, Sb) == im.getpixel((x, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y1)
                fourmi.setHeadTo('bas')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setY(y1)
                fourmi.setHeadTo('droite-bas')
            elif (Sr, Sv, Sb) == im.getpixel((x0, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x0)
                fourmi.setY(y1)
                fourmi.setHeadTo('gauche-bas')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x1)
                    fourmi.setY(y1)
                    fourmi.setHeadTo('droite-bas')
                elif direction == 'd':
                    fourmi.setX(x0)
                    fourmi.setY(y1)
                    fourmi.setHeadTo('gauche-bas')
                else:
                    fourmi.setY(y1)
                    fourmi.setHeadTo('bas')
        else:
            if (Sr, Sv, Sb) == im.getpixel((x1, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setY(y1)
                fourmi.setHeadTo('droite-bas')
            elif (Sr, Sv, Sb) == im.getpixel((x1, y)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setX(x1)
                fourmi.setHeadTo('droite')
            elif (Sr, Sv, Sb) == im.getpixel((x, y1)) and fourmi.getProbabiliteSuivi() != 0:
                fourmi.setY(y1)
                fourmi.setHeadTo('bas')
            else:
                direction = random_pick('gdt', [Pg, Pd, Pt])
                if direction == 'g':
                    fourmi.setX(x1)
                    fourmi.setHeadTo('droite')
                elif direction == 'd':
                    fourmi.setY(y1)
                    fourmi.setHeadTo('bas')
                else:
                    fourmi.setX(x1)
                    fourmi.setY(y1)
                    fourmi.setHeadTo('droite-bas')

def peinture(fourmi,i,taille,im):
    print('Ant-%d is drawing...'%i)
    iteration = 0
    while iteration<1000000:
        pix = im.load()
        Cr = fourmi.getCouleurDeposee()[0]
        Cv = fourmi.getCouleurDeposee()[1]
        Cb = fourmi.getCouleurDeposee()[2]

        x = fourmi.getX()
        y = fourmi.getY()
        pix[x, y] = (Cr, Cv, Cb)

        tourner(fourmi, x, y, taille)

        iteration += 1


if __name__ == '__main__':
    toile = readToileFromXml()
    taille = ''
    nbrFourmis = 0
    for t in toile:
        taille = t.getElementsByTagName('Taille')[0].childNodes[0].data
        nbrFourmis = t.getElementsByTagName('NombreFourmis')[0].childNodes[0].data

    listFourmis = []
    for i in range(int(nbrFourmis)):
        fourmi = Fourmi()
        listFourmis.append(fourmi)

    initializeFourmis(listFourmis)

    print('Start to draw in multi-thread...There are %d ants in the process!' % len(listFourmis))
    im = Image.new('RGB',(int(taille.split('*')[0]),int(taille.split('*')[1])),(255,255,255))

    threads = []
    for i in range(0,len(listFourmis)):
        t = threading.Thread(target=peinture,args=(listFourmis[i],i,taille, im))
        threads.append(t)
    for t in threads:
        t.setDaemon(True)
        t.start()
    for t in threads:
        t.join()
    im.show()
    print('All ants finish drawing!')

